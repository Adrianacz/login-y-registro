const express = require('express')
const tasks = express.Router()
const cors = require('cors')

const Task = require('../models/Task')
tasks.use(cors())


tasks.post('/register', (req, res) => {
  console.log(req.body);
  const taskData = {
    nombre: req.body.nombre,
    fecha: req.body.fecha,
    prioridad: req.body.prioridad, 
    id_usuario: req.body.id_usuario
  }
  Task.findOne({
    where: {
      nombre: req.body.nombre
    }
  })
    .then(tarea => {
      if (!tarea) {
        Task.create(taskData)
          .catch(err => {
            res.send('error: ' + err)
          })
      } else {
        res.json({ error: 'Tarea registrada' })
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})


tasks.get('/list/:id', (req, res) => {
  console.log("-----------"+req.params.id);
  Task.findAll({
    where: {
      id_usuario: req.params.id
    }
  })
    .then(task => {
      if (task) {
        res.json(task)
      } else {
        res.send('Tareas')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

tasks.get('/delete/:id', (req, res) => {
  Task.destroy({
    where: {
      id_tarea: req.params.id 
    }
  })
    .then(task => {
      if (task) {
        res.json(task)
      } else {
        res.send('Tareas')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

module.exports = tasks
