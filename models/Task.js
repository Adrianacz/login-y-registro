const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'to_tarea',
  {
    id_tarea: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: Sequelize.STRING
    },
    fecha: {
      type: Sequelize.DATE
    },
    prioridad: {
      type: Sequelize.STRING
    },
    id_usuario: {
      type: Sequelize.INTEGER
    }
  },
  {
    timestamps: false
  }
)
